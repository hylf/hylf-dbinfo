/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

import com.vanniktech.maven.publish.JavaLibrary
import com.vanniktech.maven.publish.JavadocJar
import com.vanniktech.maven.publish.SonatypeHost

plugins {
    id("java")
    id("com.vanniktech.maven.publish") version "0.28.0"
}

group = "io.github.huayunliufeng"
version = "0.0.3"
description = "传入数据库Connection对象, 查询该数据库各种基本信息, 所有属性已经封装到JavaBean中。"

repositories {
    maven("https://maven.aliyun.com/nexus/content/groups/public")
    mavenCentral()
    mavenLocal()
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.10.2")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.10.2")
    testRuntimeOnly("org.junit.platform:junit-platform-launcher:1.10.2")
    implementation("io.github.huayunliufeng:hylf-common:0.0.4-1")
    annotationProcessor("org.projectlombok:lombok:1.18.32")
    implementation("org.yaml:snakeyaml:2.2")

    testImplementation("mysql:mysql-connector-java:8.0.33")
}

tasks.test {
    useJUnitPlatform()
}

// 打包 sourcesJar 任务
val sourcesJar by tasks.registering(Jar::class) {
    dependsOn("classes")
    archiveClassifier = "sources"
    from(sourceSets.getByName("main").allSource)
}

tasks.javadoc {
    options {
        encoding = "UTF-8"
        charset("UTF-8")
    }
    (options as StandardJavadocDocletOptions).tags(
        "datetime",
        "date_time",
        "author",
    )
}

mavenPublishing {
    // 配置要发布的内容
    configure(
        JavaLibrary(
            // configures the -javadoc artifact, possible values:
            // - `JavadocJar.None()` don't publish this artifact
            // - `JavadocJar.Empty()` publish an emprt jar
            // - `JavadocJar.Javadoc()` to publish standard javadocs
            javadocJar = JavadocJar.Javadoc(),
            // whether to publish a sources jar
            sourcesJar = true,
        )
    )

    // 要在使用 gpg 2.1 或更高版本时生成 secring.gpg 文件，请使用：
    // gpg --export-secret-keys -o ~/.gnupg/secring.gpg

    // 配置maven中心
    publishToMavenCentral(SonatypeHost.CENTRAL_PORTAL)
    signAllPublications()

    // 配置 POM
    coordinates(project.group.toString(), rootProject.name, project.version.toString())
    pom {
        name.set(project.name)
        description.set(project.description)
        url.set("https://gitee.com/hylf/hylf-dbinfo")
        licenses {
            license {
                name.set("The MIT License")
                url.set("https://opensource.org/licenses/MIT")
                distribution.set("https://opensource.org/licenses/MIT")
            }
        }
        developers {
            developer {
                id.set("huayunliufeng")
                name.set("huayunliufeng")
                email.set("huayunliufeng@163.com")
                url.set("https://gitee.com/hylf")
                organization.set("io.github.huayunliufeng")
                organizationUrl.set("https://gitee.com/hylf")
            }
        }

        scm {
            connection.set("scm:git:git://gitee.com/hylf/hylf-dbinfo.git")
            developerConnection.set("scm:git:ssh://gitee.com:hylf/hylf-dbinfo.git")
            url.set("https://gitee.com/hylf/hylf-dbinfo")
        }
    }
}