/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.utils;

import io.github.huayunliufeng.common.entity.ConnectionProp;
import io.github.huayunliufeng.common.utils.HylfFunUtil;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * 数据库连接工具。
 *
 * @author huayunliufeng
 * @date_time 2024/3/22 0:43
 */
@Slf4j
public class HylfConnectionUtil {
    public static Connection getConnection(ConnectionProp prop) {
        String logFormat = "获取数据库连接失败。[prop = {}]";
        Connection conn = HylfFunUtil.methodVoidReturnExec(() -> {
            Class.forName(prop.getDriverClassName());
            return DriverManager.getConnection(prop.getUrl(), prop.getUsername(), prop.getPassword());
        }, logFormat, prop);
        if (conn == null) {
            System.exit(1);
        }
        return conn;
    }
}
