/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.utils;

import io.github.huayunliufeng.common.entity.ConnectionProp;
import io.github.huayunliufeng.common.utils.HylfCollectionUtil;
import io.github.huayunliufeng.common.utils.HylfDataUtil;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 读取数据源工具, 数据源配置文件必须是datasource.yml。
 *
 * @author huayunliufeng
 * @date_time 2024/3/21 21:47
 */
@Slf4j
public class ReadConfigUtil {
    public static final String CONFIG_NAME = "datasource.yml";
    private static Map<String, Object> configs = null;

    /**
     * 读取所有数据源, 其中key是数据源名称
     *
     * @return 所有数据源的map集合
     */
    @SuppressWarnings("unchecked")
    public static Map<String, ConnectionProp> readAllDatasource() {
        try {
            Yaml yaml = new Yaml();
            if (configs == null) {
                configs = yaml.load(ReadConfigUtil.class.getClassLoader().getResourceAsStream(CONFIG_NAME));
            }
            List<Map<String, Map<String, Object>>> datasource = (List<Map<String, Map<String, Object>>>) configs.get(
                    "datasource");
            Map<String, ConnectionProp> resultMap = new HashMap<>();
            for (Map<String, Map<String, Object>> map : datasource) {
                for (String key : map.keySet()) {
                    Map<String, Object> stringStringMap = map.get(key);
                    if (HylfCollectionUtil.isEmpty(stringStringMap)) {
                        throw new RuntimeException("配置为空。");
                    }
                    ConnectionProp connectionProp = HylfDataUtil.mapToJavaBean(stringStringMap, ConnectionProp.class);
                    resultMap.put(key, connectionProp);
                    break;
                }
            }
            log.debug("读取到的配置：[resultMap = {}]", resultMap);
            return resultMap;
        } catch (Exception e) {
            log.error("读取数据源配置失败。[configName = {}]", CONFIG_NAME, e);
            System.exit(1);
        }
        return null;
    }

    /**
     * 读取指定的数据源
     *
     * @param datasourceName 数据源名称
     * @return 数据源配置
     */
    public static ConnectionProp readDatasource(String datasourceName) {
        Map<String, ConnectionProp> propMap = readAllDatasource();
        if (HylfCollectionUtil.isEmpty(propMap)) {
            System.exit(1);
        }
        return propMap.get(datasourceName);
    }
}
