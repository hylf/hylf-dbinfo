/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.constant;

import lombok.Getter;

/**
 * 数据库表类型。
 *
 * @author zhongq
 * @datetime 2024/3/26 10:18
 */
@Getter
public enum TableType {
    TABLE("TABLE", "表"),
    VIEW("VIEW", "视图"),
    SYSTEM_TABLE("SYSTEM TABLE", "系统表"),
    SYSTEM_VIEW("SYSTEM VIEW", "系统视图"),
    GLOBAL_TEMPORARY("GLOBAL TEMPORARY", "全局临时"),
    LOCAL_TEMPORARY("LOCAL TEMPORARY", "本地临时"),
    ALIAS("ALIAS", "别名"),
    SYSNONYM("SYSNONYM", "同义词");
    private final String code;
    private final String info;

    TableType(String code, String info) {
        this.code = code;
        this.info = info;
    }
}
