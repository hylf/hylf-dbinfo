/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.dbinfo;

import io.github.huayunliufeng.common.utils.HylfDataUtil;
import io.github.huayunliufeng.common.utils.HylfFunUtil;
import io.github.huayunliufeng.dbinfo.model.TablePrimaryKeyInfo;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

/**
 * 表的主键信息。
 *
 * @author zhongq
 * @datetime 2024/3/26 17:06
 */
@Slf4j
public class ObtainPrimaryKeyInfo extends ObtainDbInfo {

    private static Map<Connection, ObtainPrimaryKeyInfo> obtainPrimaryKeyMap = null;

    private Map<String, List<TablePrimaryKeyInfo>> primaryKeyInfoMap = null;

    protected ObtainPrimaryKeyInfo(Connection conn) {
        super(conn);
    }

    public static ObtainPrimaryKeyInfo build(Connection conn) {
        obtainPrimaryKeyMap = HylfFunUtil.mapAddValue(obtainPrimaryKeyMap, conn, () -> new ObtainPrimaryKeyInfo(conn));
        return obtainPrimaryKeyMap.get(conn);
    }

    /**
     * 检索给定表的主键列的描述。它们按COLUMN_NAME排序。
     *
     * @param catalog 目录名称;必须与存储在数据库中的目录名称匹配;“”检索那些没有目录的;null表示不应使用目录名称来缩小搜索范围
     * @param schema  模式名;必须与存储在数据库中的模式名称匹配;""检索那些没有模式的;null表示不应该使用模式名称来缩小搜索范围
     * @param table   表名;必须匹配存储在数据库中的表名
     * @return 每一行都是一个主键列描述
     */
    public List<TablePrimaryKeyInfo> getPrimaryKeys(String catalog, String schema, String table) {
        String logFormat = "获取数据库表主键信息失败。[catalog = {}, schema = {}, table = {}]";
        return HylfFunUtil.methodVoidReturnExec(() -> {
            String key = String.join("-", catalog, schema, table);
            primaryKeyInfoMap = HylfFunUtil.mapAddValue(primaryKeyInfoMap, key, () -> {
                ResultSet resultSet = HylfFunUtil.methodVoidReturnExec(
                        () -> databaseMetaData.getPrimaryKeys(catalog, schema, table)
                        , logFormat, catalog, schema, table);
                List<TablePrimaryKeyInfo> primaryKeyInfoList = HylfDataUtil.autoSetValue(resultSet, TablePrimaryKeyInfo.class);
                HylfFunUtil.autoClose(resultSet);
                return primaryKeyInfoList;
            });
            return primaryKeyInfoMap.get(key);
        });
    }

    /**
     * 检索给定表的主键列的描述。它们按COLUMN_NAME排序。
     *
     * @param table 表名;必须匹配存储在数据库中的表名
     * @return 每一行都是一个主键列描述
     */
    public List<TablePrimaryKeyInfo> getPrimaryKeys(String table) {
        return getPrimaryKeys(getCatalog(), getSchema(), table);
    }
}
