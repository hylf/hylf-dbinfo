/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import io.github.huayunliufeng.common.annotation.InjectIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 给定目录中可用的存储过程的描述。
 *
 * @author zhongq
 * @datetime 2024/4/1 10:54
 */
@Getter
public class ProceduresInfo {
    /**
     * 过程目录(可能为空)
     */
    private String procedureCat;

    /**
     * 过程模式(可能为null)
     */
    private String procedureSchem;

    /**
     * 过程名
     */
    private String procedureName;

    // reserved for future use
    // reserved for future use
    // reserved for future use

    /**
     * 对过程的解释性注释
     */
    private String remarks;

    /**
     * 过程类型:
     * <ul>
     * <li>procedureResultUnknown - 无法确定是否有返回值</li>
     * <li>procedureNoResult - 无返回值</li>
     * <li>procedureReturnsResult - 返回一个值</li>
     * </ul>
     */
    private short procedureType;

    /**
     * 在其模式中唯一标识此过程的名称。
     */
    private String specificName;

    /**
     * 给定目录的存储过程参数和结果列的描述。
     */
    @InjectIgnore
    @Setter
    private List<ProcedureColumnsInfo> procedureColumnsInfoList;
}
