/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import lombok.Getter;

/**
 * 数据库的特定模式中定义的表层次结构的描述。
 *
 * @author zhongq
 * @datetime 2024/4/1 16:09
 */
@Getter
public class SuperTablesInfo {
    /**
     * 类型的目录(可能为null)
     */
    private String tableCat;

    /**
     * 类型的模式(可能为null)
     */
    private String tableSchem;

    /**
     * 类型名称
     */
    private String tableName;

    /**
     * 直接超类型的名称
     */
    private String supertableName;
}
