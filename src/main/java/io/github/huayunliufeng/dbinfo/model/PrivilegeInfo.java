/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import lombok.Getter;

/**
 * 表列的访问权限描述。
 *
 * @author zhongq
 * @datetime 2024/3/28 15:50
 */
@Getter
public class PrivilegeInfo {
    /**
     * 表目录(可能为空)
     */
    private String tableCat;

    /**
     * 表模式(可能为空)
     */
    private String tableSchem;

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 字段名
     */
    private String columnName;

    /**
     * 权限授予人(可以为null)
     */
    private String grantor;

    /**
     * 访问权限被授予人
     */
    private String grantee;

    /**
     * 访问权限(SELECT, INSERT, UPDATE, REFERENCES，…)
     */
    private String privilege;

    /**
     * "YES"，如果被授予人被允许授予他人;如果不是，“NO”;如果未知则为空
     */
    private String isGrantable;
}
