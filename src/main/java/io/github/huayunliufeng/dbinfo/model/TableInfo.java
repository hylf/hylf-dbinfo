/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import io.github.huayunliufeng.common.annotation.InjectIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 数据库表信息。
 *
 * @author zhongq
 * @datetime 2024/3/26 13:36
 */
@Getter
public class TableInfo {
    /**
     * 表目录(可能为空)
     */
    private String tableCat;

    /**
     * 表模式(可能为空)
     */
    private String tableSchem;

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 表类型。典型的类型有 "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL TEMPORARY", "ALIAS", "SYNONYM".
     */
    private String tableType;

    /**
     * 表上的解释性注释(可能为空)
     */
    private String remarks;

    /**
     * 类型目录(可能为空)
     */
    private String typeCat;

    /**
     * 类型模式(可能为空)
     */
    private String typeSchem;

    /**
     * 类型名称(可能为空)
     */
    private String typeName;

    /**
     * 类型化表的指定“identifier”列的名称(可以为空)。
     */
    private String selfReferencingColName;

    /**
     * 指定如何创建SELF_REFERENCING_COL_NAME中的值。取值为SYSTEM、USER、DERIVED。(可能为空)
     */
    private String refGeneration;

    /**
     * 表的字段信息
     */
    @InjectIgnore
    @Setter
    private List<TableColumnInfo> columnInfoList;

    /**
     * 表的主键信息
     */
    @InjectIgnore
    @Setter
    private List<TablePrimaryKeyInfo> primaryKeyInfoList;

    /**
     * 表的索引信息
     */
    @InjectIgnore
    @Setter
    private List<TableIndexInfo> indexInfoList;

    /**
     * 表的外键信息
     */
    @InjectIgnore
    @Setter
    private List<TableExportedKeyInfo> exportedKeyInfoList;

    /**
     * 表的访问权限信息
     */
    @InjectIgnore
    @Setter
    private List<PrivilegeInfo> privilegesInfoList;

    /**
     * 数据库的特定模式中定义的表层次结构的描述。
     */
    @InjectIgnore
    @Setter
    private List<SuperTablesInfo> superTablesInfoList;

    /**
     * 在更新行中的任何值时自动更新的表列的描述。
     */
    @InjectIgnore
    @Setter
    private List<VersionColumnsInfo> versionColumnsInfoList;
}
