/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import lombok.Getter;

/**
 * 表中唯一标识一行的最佳列集的描述。
 *
 * @author zhongq
 * @datetime 2024/3/29 13:56
 */
@Getter
public class BestRowIdentifierInfo {
    /**
     * 结果的实际范围.
     * <ul>
     * <li>bestRowTemporary - 非常临时，而使用row</li>
     * <li>bestRowTransaction - 对当前事务的剩余部分有效</li>
     * <li>bestRowSession - 对当前会话的剩余部分有效</li>
     * </ul>
     */
    private short scope;

    /**
     * 列名
     */
    private String columnName;

    /**
     * SQL数据类型从java.sql.Types
     */
    private int dataType;

    /**
     * 依赖于数据源的类型名称，对于UDT，类型名称是完全限定的
     */
    private String typeName;

    /**
     * <p>COLUMN_SIZE列表示给定列的指定列大小。</p>
     * <p>对于数字数据，这是最大精度。</p>
     * <p>对于字符数据，这是以字符为单位的长度。</p>
     * <p>对于日期时间数据类型，这是字符串表示的长度（以字符为单位）（假设分数秒组件的最大允许精度）。</p>
     * <p>对于二进制数据，这是以字节为单位的长度。</p>
     * <p>对于ROWID数据类型，这是以字节为单位的长度。</p>
     * <p>对于列大小不适用的数据类型，返回null。
     */
    private int columnSize;

    /**
     * 未使用
     */
    private int bufferLength;

    /**
     * scale-对于不适用DECIMAL_DIGITS的数据类型返回null。
     */
    private short decimalDigits;

    /**
     * 是类似于Oracle ROWID的伪列。
     * <ul>
     * <li>bestRowUnknown - 可能是也可能不是伪列</li>
     * <li>bestRowNotPseudo - 不是一个伪列</li>
     * <li>bestRowPseudo - 是一个伪列</li>
     * </ul>
     */
    private short pseudoColumn;
}
