/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import lombok.Getter;

/**
 * 在更新行中的任何值时自动更新的表列的描述。
 *
 * @author zhongq
 * @datetime 2024/4/1 16:57
 */
@Getter
public class VersionColumnsInfo {
    /**
     * 没有被使用
     */
    private short scope;

    /**
     * 列名
     */
    private String columnName;

    /**
     * SQL数据类型从java.sql.Types
     */
    private int dataType;

    /**
     * 依赖于数据源的类型名称
     */
    private String typeName;

    /**
     * <p>COLUMN_SIZE列表示给定列的指定列大小。</p>
     * <p>对于数字数据，这是最大精度。</p>
     * <p>对于字符数据，这是以字符为单位的长度。</p>
     * <p>对于日期时间数据类型，这是字符串表示的长度（以字符为单位）（假设分数秒组件的最大允许精度）。</p>
     * <p>对于二进制数据，这是以字节为单位的长度。</p>
     * <p>对于ROWID数据类型，这是以字节为单位的长度。</p>
     * <p>对于列大小不适用的数据类型，返回null。</p>
     */
    private int columnSize;

    /**
     * 列值的长度，以字节为单位
     */
    private int bufferLength;

    /**
     * 对于不适用DECIMAL_DIGITS的数据类型返回null。
     */
    private short decimalDigits;

    /**
     * 是否是像Oracle ROWID那样的伪列。
     * <ul>
     * <li>versionColumnUnknown - 可能是也可能不是伪列</li>
     * <li>versionColumnNotPseudo - 不是伪列</li>
     * <li>versionColumnPseudo - 伪列</li>
     * </ul>
     */
    private short pseudoColumn;
}
