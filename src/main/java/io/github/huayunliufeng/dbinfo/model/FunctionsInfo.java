/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import io.github.huayunliufeng.common.annotation.InjectIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 给定目录中可用的系统和用户功能的描述。
 *
 * @author zhongq
 * @datetime 2024/4/1 10:04
 */
@Getter
public class FunctionsInfo {
    /**
     * 函数目录(可能为null)
     */
    private String functionCat;

    /**
     * 函数模式(可能为null)
     */
    private String functionSchem;

    /**
     * 函数名。这是用于调用函数的名称
     */
    private String functionName;

    /**
     * 对函数的解释性注释
     */
    private String remarks;

    /**
     * 函数类型:
     * <ul>
     * <li>functionResultUnknown - 无法确定是否返回返回值或表</li>
     * <li>functionReturnsTable - 返回一个表</li>
     * </ul>
     */
    private short functionType;

    /**
     * 在其模式中唯一标识该函数的名称。这是用户指定的或DBMS生成的名称，它可能与FUNCTION_NAME(例如重载函数)不同
     */
    private String specificName;

    /**
     * 给定目录的系统或用户函数参数和返回类型的描述。
     */
    @InjectIgnore
    @Setter
    private List<FunctionColumnInfo> functionColumnInfoList;
}
