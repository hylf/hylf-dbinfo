/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import lombok.Getter;

/**
 * 此数据库中的特定模式中定义的用户定义类型(UDT)层次结构的描述。只对直接的父类型/子类型关系建模。
 *
 * @author zhongq
 * @datetime 2024/3/28 16:50
 */
@Getter
public class SuperTypesInfo {
    /**
     * 表目录(可能为空)
     */
    private String tableCat;

    /**
     * 表模式(可能为空)
     */
    private String tableSchem;

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 直接超类型的目录(可能为null)
     */
    private String supertypeCat;

    /**
     * 直接超类型的模式(可能为null)
     */
    private String supertypeSchema;

    /**
     * 直接超类型的名称
     */
    private String supertypeName;
}
