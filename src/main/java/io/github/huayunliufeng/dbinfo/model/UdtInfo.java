/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import lombok.Getter;

/**
 * 特定模式中定义的用户定义类型(UDTs)的描述。
 *
 * @author zhongq
 * @datetime 2024/4/1 16:43
 */
@Getter
public class UdtInfo {
    /**
     * 类型的目录(可能为null)
     */
    private String typeCat;

    /**
     * 类型的模式(可能为null)
     */
    private String typeSchem;

    /**
     * 类型名称
     */
    private String typeName;

    /**
     * Java类名
     */
    private String className;

    /**
     * java.sql.Types中定义的类型值。JAVA_OBJECT、STRUCT或DISTINCT之一
     */
    private int dataType;

    /**
     * 对类型的解释性注释
     */
    private String remarks;

    /**
     * 一个DISTINCT类型的源类型的类型代码，或者实现用户生成的引用类型的self - referencing_column的引用类型的类型，
     * 如java.sql.Types中定义的(如果DATA_TYPE不是DISTINCT或不是STRUCT with REFERENCE_GENERATION = USER_DEFINED则为空)
     */
    private short baseType;
}
