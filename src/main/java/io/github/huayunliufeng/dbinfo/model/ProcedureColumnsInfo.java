/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import lombok.Getter;

/**
 * 给定目录的存储过程参数和结果列的描述。
 *
 * @author zhongq
 * @datetime 2024/4/1 11:31
 */
@Getter
public class ProcedureColumnsInfo {
    /**
     * 过程目录(可能为空)
     */
    private String procedureCat;

    /**
     * 过程模式(可能为null)
     */
    private String procedureSchem;

    /**
     * 过程名
     */
    private String procedureName;

    /**
     * 列名/参数名
     */
    private String columnName;

    /**
     * 列/参数的类型：
     * <ul>
     * <li>procedureColumnUnknown - 未知</li>
     * <li>procedureColumnIn - IN参数</li>
     * <li>procedureColumnInOut - INOUT参数</li>
     * <li>procedureColumnOut - OUT参数</li>
     * <li>procedureColumnReturn - 过程返回值</li>
     * <li>procedureColumnResult - ResultSet的结果列
     * </ul>
     */
    private short columnType;

    /**
     * SQL类型: java.sql.Types
     */
    private int dataType;

    /**
     * SQL类型名称，对于UDT类型，类型名称是完全限定的
     */
    private String typeName;

    /**
     * <p>表示给定列的指定列大小。</p>
     * <p>对于数字数据，这是最大精度。</p>
     * <p>对于字符数据，这是以字符为单位的长度。</p>
     * <p>对于日期时间数据类型，这是字符串表示的长度（以字符为单位）（假设分数秒组件的最大允许精度）。</p>
     * <p>对于二进制数据，这是以字节为单位的长度。</p>
     * <p>对于ROWID数据类型，这是以字节为单位的长度。</p>
     * <p>对于列大小不适用的数据类型，返回null。</p>
     */
    private int precision;

    /**
     * 数据的字节长度
     */
    private int length;

    /**
     * 对于scale不适用的数据类型返回null。
     */
    private short scale;

    /**
     * 基数
     */
    private short radix;

    /**
     * 是否可以包含NULL。
     * <ul>
     * <li>procedureNoNulls - 可能不允许<code>NULL</code>值</li>
     * <li>procedureNullable - 绝对允许<code>NULL</code>值</li>
     * <li>procedureNullableUnknown - 可空性未知</li>
     * </ul>
     */
    private short nullable;

    /**
     * 描述参数/列的注释
     */
    private String remarks;

    /**
     * 列的默认值，当值被括在单引号中时(可以是null)，它应该被解释为字符串。
     * <ul>
     * <li>字符串NULL（不加引号） - 如果将NULL指定为默认值</li>
     * <li>TRUNCATE（不括在引号中） - 如果指定的默认值不能在没有截断的情况下表示</li>
     * <li>NULL - 如果未指定默认值</li>
     * </ul>
     */
    private String columnDef;

    /**
     * 保留供将来使用
     */
    private int sqlDataType;

    /**
     * 保留以供将来使用
     */
    private int sqlDatetimeSub;

    /**
     * 基于二进制和字符的列的最大长度。对于任何其他数据类型，返回值为NULL
     */
    private int charOctetLength;

    /**
     * <p>过程的输入和输出参数的顺序位置，从1开始。</p>
     * <p>如果这一行描述了过程的返回值，则返回值0。</p>
     * <p>对于结果集列，它是从1开始的列在结果集中的顺序位置。</p>
     * <p>如果有多个结果集，则列顺序位置由实现定义。</p>
     */
    private int ordinalPosition;

    /**
     * ISO规则用于确定列的可空性。
     * <ul>
     * <li>YES--如果属性可以包含null</li>
     * <li>NO--如果属性不能包含null</li>
     * <li>空字符串--如果属性的可空性未知</li>
     * </ul>
     */
    private String isNullable;

    /**
     * 在其模式中唯一标识此过程的名称。
     */
    private String specificName;
}
