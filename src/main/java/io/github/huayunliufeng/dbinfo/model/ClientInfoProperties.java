/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import lombok.Getter;

/**
 * 驱动程序支持的客户端信息属性列表。
 *
 * @author zhongq
 * @datetime 2024/3/28 14:31
 */
@Getter
public class ClientInfoProperties {
    /**
     * 客户端info属性名称
     */
    private String name;

    /**
     * 属性值的最大长度
     */
    private int maxLen;

    /**
     * 属性的默认值
     */
    private String defaultValue;

    /**
     * 属性的描述信息。这通常包含有关此属性在数据库中的存储位置的信息。
     */
    private String description;
}
