/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

/**
 * 表索引信息。
 *
 * @author huayunliufeng
 * @date_time 2024/3/26 20:59
 */
public class TableIndexInfo {
    /**
     * 表目录(可能为空)
     */
    private String tableCat;

    /**
     * 表模式(可能为空)
     */
    private String tableSchem;

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 索引值可以是非唯一的。当TYPE为<b>tableIndexStatistic</b>时，返回<code>false</code>
     */
    private boolean nonUnique;

    /**
     * 索引目录(可以为null);当TYPE为<b>tableIndexStatistic</b>时为<code>null</code>
     */
    private String indexQualifier;

    /**
     * 索引名称;当TYPE为<b>tableIndexStatistic</b>时为<code>null</code>
     */
    private String indexName;

    /**
     * 索引类型：
     * <ul>
     * <li>tableIndexStatistic - 标识与表的索引描述一起返回的表统计信息</li>
     * <li>tableIndexClustered - 这是一个聚集索引</li>
     * <li>tableIndexHashed - 这是一个散列索引</li>
     * <li>tableIndexOther - 这是另一种类型的索引</li>
     * </ul>
     */
    private short type;

    /**
     * 索引内的列序号;当TYPE为<b>tableIndexStatistic</b>时为0
     */
    private short ordinalPosition;

    /**
     * 列名称;当TYPE为<b>tableIndexStatistic</b>时为<code>null</code>
     */
    private String columnName;

    /**
     * 列排序序列，"A" =>升序，"D" =>降序，如果不支持排序序列，可能为<code>null</code>;当TYPE为<b>tableIndexStatistic</b>时为<code>null</code>
     */
    private String ascOrDesc;

    /**
     * 当TYPE为<b>tableIndexStatistic</b>时，这是表的行数;否则，它是索引中唯一值的数量。
     */
    private long cardinality;

    /**
     * 当TYPE为<b>tableIndexStatistic</b>时，该值为表的页数，否则为当前索引的页数。
     */
    private long pages;

    /**
     * 过滤条件(如果有的话)。(可能为null)
     */
    private String filterCondition;
}
