/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import lombok.Getter;

import java.sql.PseudoColumnUsage;

/**
 * 指定目录和模式中给定表中可用的伪列或隐藏列的描述。
 *
 * @author zhongq
 * @datetime 2024/4/1 10:28
 */
@Getter
public class PseudoColumnsInfo {
    /**
     * 表目录(可能为空)
     */
    private String tableCat;

    /**
     * 表模式(可能为空)
     */
    private String tableSchem;

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 字段名
     */
    private String columnName;

    /**
     * SQL type from java.sql.Types
     */
    private int dataType;

    /**
     * 列大小。
     */
    private int columnSize;

    /**
     * 小数位数的个数。对于不适用DECIMAL_DIGITS的数据类型返回null。
     */
    private int decimalDigits;

    /**
     * 基数(通常为10或2)
     */
    private int numPrecRadix;

    /**
     * 允许使用的列。返回的值将对应于{@link PseudoColumnUsage#name PseudoColumnUsage.name()}返回的enum名称。
     */
    private String columnUsage;

    /**
     * 注释描述列(可能为空)
     */
    private String remarks;

    /**
     * 对于char类型，列的最大字节数
     */
    private int charOctetLength;

    /**
     * ISO规则用于确定列的可空性。
     * <ul>
     * <li>YES - 如果列可以包含null</li>
     * <li>NO - 如果列不能包含null</li>
     * <li>空字符串 - 如果列的可空性未知</li>
     * </ul>
     */
    private String isNullable;
}
