/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import io.github.huayunliufeng.common.annotation.InjectIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 数据库表字段信息。
 *
 * @author zhongq
 * @datetime 2024/3/26 14:53
 */
@Getter
public class TableColumnInfo {
    /**
     * 表目录(可能为空)
     */
    private String tableCat;

    /**
     * 表模式(可能为空)
     */
    private String tableSchem;

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 字段名
     */
    private String columnName;

    /**
     * SQL type from java.sql.Types
     */
    private int dataType;

    /**
     * 依赖于数据源的类型名称，对于UDT，类型名称是完全限定的
     */
    private String typeName;

    /**
     * 字段大小
     */
    private int columnSize;

    /**
     * 没有使用
     */
    private int bufferLength;

    /**
     * 小数位数的数目。对于不适用DECIMAL_DIGITS的数据类型返回null。
     */
    private int decimalDigits;

    /**
     * 基数(通常为10或2)
     */
    private int numPrecRadix;

    /**
     * <ul>
     * <li>columnNoNulls - 可能不允许NULL值</li>
     * <li>columnNullable - 绝对允许NULL值</li>
     * <li>columnNullableUnknown - 空性未知</li>
     * </ul>
     */
    private int nullable;

    /**
     * 字段注释(可能为空)
     */
    private String remarks;

    /**
     * 列的默认值，当值被括在单引号中时(可能为空)，它应该被解释为字符串。
     */
    private String columnDef;

    /**
     * 未使用
     */
    private int sqlDataType;

    /**
     * 未使用
     */
    private int sqlDatetimeSub;

    /**
     * 对于char类型，列中的最大字节数
     */
    private int charOctetLength;

    /**
     * 表中列的索引(从1开始)
     */
    private int ordinalPosition;

    /**
     * ISO规则用于确定列的可空性。
     * <ul>
     * <li>YES - 如果列可以包含null</li>
     * <li>NO - 如果列不能包含null</li>
     * <li>空字符串 - 如果列的可空性未知</li>
     * </ul>
     */
    private String isNullable;

    /**
     * 表的目录，它是引用属性的作用域(如果DATA_TYPE不是REF则为空)
     */
    private String scopeCatalog;

    /**
     * 作为引用属性范围的表模式(如果DATA_TYPE不是REF则为空)
     */
    private String scopeSchema;

    /**
     * 表名表示引用属性的作用域(如果DATA_TYPE不是REF则为空)
     */
    private String scopeTable;

    /**
     * DATA_TYPE不是distinct类型或用户生成的Ref类型，SQL类型来自java.sql.Types(如果DATA_TYPE不是distinct或用户生成的Ref，则为空)
     */
    private short sourceDataType;

    /**
     * 指示该列是否自动递增
     * <ul>
     * <li>YES - 如果列是自动递增的</li>
     * <li>NO - 如果列不是自动递增的</li>
     * <li>空字符串 - 如果不能确定列是否自动递增</li>
     * </ul>
     */
    private String isAutoincrement;

    /**
     * 指示这是否是生成的列
     * <ul>
     * <li>YES - 如果这是一个生成的列</li>
     * <li>NO - 如果这不是一个生成的列</li>
     * <li>空字符串 - 如果无法确定这是否是生成的列</li>
     * </ul>
     */
    private String isGeneratedcolumn;

    @InjectIgnore
    @Setter
    private List<PrivilegeInfo> columnPrivilegeInfoList;
}
