/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import lombok.Getter;

/**
 * 表的外键信息。
 *
 * @author zhongq
 * @datetime 2024/3/27 17:05
 */
@Getter
public class TableExportedKeyInfo {
    /**
     * 主键表目录(可能为空)
     */
    private String pktableCat;

    /**
     * 主键表模式(可能为空)
     */
    private String pktableSchem;

    /**
     * 主键表名
     */
    private String pktableName;

    /**
     * 主键列名
     */
    private String pkcolumnName;

    /**
     * 正在导出的外键表目录(可能为空)
     */
    private String fktableCat;

    /**
     * 正在导出的外键表模式(可能为空)
     */
    private String fktableSchem;

    /**
     * 正在导出的外键表名
     */
    private String fktableName;

    /**
     * 正在导出的外键列名
     */
    private String fkcolumnName;

    /**
     * 外键内的序列号(值1表示外键的第一列，值2表示外键内的第二列)。
     */
    private short keySeq;

    /**
     * 更新主键时外键会发生什么:
     * <ul>
     * <li>importedNoAction - 如果主键已被导入，则不允许更新主键</li>
     * <li>importedKeyCascade - 更改导入键以与主键更新一致</li>
     * <li>importedKeySetNull - 如果主键已更新，则将导入键更改为<code>NULL</code></li>
     * <li>importedKeySetDefault - 如果主键已更新，则将导入键更改为默认值</li>
     * <li>importedKeyRestrict - 与<b>importedKeyNoAction</b>相同(用于odbc2)。x兼容性)</li>
     * </ul>
     */
    private short updateRule;

    /**
     * 当主键被删除时外键会发生什么。
     * <ul>
     * <li>importedKeyNoAction - 不允许删除主键，如果它已被导入</li>
     * <li>importedKeyCascade - 删除导入已删除键的行</li>
     * <li>importedKeySetNull - 如果主键已被删除，则将导入键更改为<code>NULL</code></li>
     * <li>importedKeyRestrict - 与importtedkeynoaction相同(用于odbc2)。x兼容性)</li>
     * <li>importedKeySetDefault - 如果主键已被删除，则将导入键更改为default</li>
     * </ul>
     */
    private short deleteRule;

    /**
     * 外键名(可能为空)
     */
    private String fkName;

    /**
     * 主键名(可能为空)
     */
    private String pkName;

    /**
     * 外键约束的评估是否可以推迟到提交
     * <ul>
     * <li>importedKeyInitiallyDeferred - 参见SQL92的定义</li>
     * <li>importedKeyInitiallyImmediate - 参见SQL92的定义</li>
     * <li>importedKeyNotDeferrable - 参见SQL92的定义</li>
     * </ul>
     */
    private short deferrability;
}
