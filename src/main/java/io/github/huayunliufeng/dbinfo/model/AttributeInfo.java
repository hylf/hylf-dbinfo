/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import lombok.Getter;

/**
 * 给定模式和编目中可用的用户定义类型(UDT)的给定类型的给定属性的描述。
 *
 * @author zhongq
 * @datetime 2024/3/28 16:30
 */
@Getter
public class AttributeInfo {
    /**
     * 表目录(可能为空)
     */
    private String tableCat;

    /**
     * 表模式(可能为空)
     */
    private String tableSchem;

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 属性名称
     */
    private String attrName;

    /**
     * 属性类型<code>java.sql.Types</code>中的SQL类型
     */
    private int dataType;

    /**
     * 数据源依赖类型名称。对于UDT，类型名称是完全限定的。对于REF，类型名称是完全限定的，并且表示引用类型的目标类型。
     */
    private String attrTypeName;

    /**
     * 列大小。对于char或date类型，这是最大字符数;对于数字或十进制类型，这是精度。
     */
    private int attrSize;

    /**
     * 小数位数的个数。对于不适用DECIMAL_DIGITS的数据类型返回null。
     */
    private int decimalDigits;

    /**
     * 基数(通常为10或2)
     */
    private int numPrecRadix;

    /**
     * 是否允许NULL
     * <ul>
     * <li>attributeNoNulls - 可能不允许<code>NULL</code>值</li>
     * <li>attributeNullable - 绝对允许<code>NULL</code>值</li>
     * <li>attributeNullableUnknown - 可空性未知</li>
     * </ul>
     */
    private int nullable;

    /**
     * 注释描述列(可能为空)
     */
    private String remarks;

    /**
     * 默认值(可能为null)
     */
    private String attrDef;

    /**
     * 未使用
     */
    private int sqlDataType;

    /**
     * 未使用
     */
    private int sqlDatetimeSub;

    /**
     * 对于char类型，列的最大字节数
     */
    private int charOctetLength;

    /**
     * UDT中属性的索引(从1开始)
     */
    private int ordinalPosition;

    /**
     * ISO规则用于确定属性的可空性。
     * <ul>
     * <li>YES--如果属性可以包含null</li>
     * <li>NO--如果属性不能包含null</li>
     * <li>空字符串--如果属性的可空性未知</li>
     * </ul>
     */
    private String isNullable;

    /**
     * 作为引用属性范围的表的目录(如果DATA_TYPE不是REF则为空)
     */
    private String scopeCatalog;

    /**
     * 作为引用属性范围的表名(如果DATA_TYPE不是REF则为空)
     */
    private String scopeSchema;

    /**
     * 作为引用属性范围的表名(如果DATA_TYPE不是REF则为空)
     */
    private String scopeTable;

    /**
     * 不同类型或用户生成的Ref类型的源类型，SQL类型来自java.sql.Types(如果DATA_TYPE不是distinct或用户生成的Ref，则为空)
     */
    private short sourceDataType;
}
