/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import lombok.Getter;

/**
 * 给定目录的系统或用户函数参数和返回类型的描述。
 *
 * @author zhongq
 * @datetime 2024/4/1 9:16
 */
@Getter
public class FunctionColumnInfo {
    /**
     * 函数目录(可能为null)
     */
    private String functionCat;

    /**
     * 函数模式(可能为null)
     */
    private String functionSchem;

    /**
     * 函数名。这是用于调用函数的名称
     */
    private String functionName;

    /**
     * 列/参数名称
     */
    private String columnName;

    /**
     * 列/参数的类型:
     * <ul>
     * <li>functionColumnUnknown - 没有人知道</li>
     * <li>functionColumnIn - IN参数</li>
     * <li>functionColumnInOut - INOUT参数</li>
     * <li>functionColumnOut - OUT参数</li>
     * <li>functionColumnReturn - 函数返回值</li>
     * <li>functionColumnResult - 参数或列是ResultSet中的一个列</li>
     * </ul>
     */
    private short columnType;

    /**
     * SQL类型: java.sql.Types
     */
    private int dataType;

    /**
     * SQL类型名称，对于UDT类型，类型名称是完全限定的
     */
    private String typeName;

    /**
     * <p>表示给定参数或列的指定列大小。</p>
     * <p>对于数字数据，这是最大精度。</p>
     * <p>对于字符数据，这是以字符为单位的长度。</p>
     * <p>对于日期时间数据类型，这是字符串表示的长度（以字符为单位）（假设分数秒组件的最大允许精度）。</p>
     * <p>对于二进制数据，这是以字节为单位的长度。</p>
     * <p>对于ROWID数据类型，这是以字节为单位的长度。</p>
     * <p>对于列大小不适用的数据类型，返回null。</p>
     */
    private int precision;

    /**
     * 数据的字节长度
     */
    private int length;

    /**
     * 对于scale不适用的数据类型返回null。
     */
    private short scale;

    /**
     * 基数
     */
    private short radix;

    /**
     * 是否可以包含NULL。
     * <ul>
     * <li>functionNoNulls - 不允许NULL值</li>
     * <li>functionNullable - 允许NULL值</li>
     * <li>functionNullableUnknown - 空性未知</li>
     * </ul>
     */
    private short nullable;

    /**
     * 描述列/参数的注释
     */
    private String remarks;

    /**
     * 基于二进制和字符的参数或列的最大长度。对于任何其他数据类型，返回值为NULL
     */
    private int charOctetLength;

    /**
     * 输入和输出参数的顺序位置，从1开始。
     * 如果这一行描述函数的返回值，则返回值0。对于结果集列，它是从1开始的列在结果集中的顺序位置。
     */
    private int ordinalPosition;

    /**
     * ISO规则用于确定参数或列的可空性。
     * <ul>
     * <li>YES - 如果参数或列可以包含null</li>
     * <li>NO - 如果参数或列不能包含null</li>
     * <li>空字符串 - 如果参数或列的可空性未知</li>
     * </ul>
     */
    private String isNullable;

    /**
     * 在其模式中唯一标识该函数的名称。这是用户指定的或DBMS生成的名称，它可能与FUNCTION_NAME(例如重载函数)不同
     */
    private String specificName;
}
