/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import lombok.Getter;

/**
 * 表的主键信息。
 *
 * @author zhongq
 * @datetime 2024/3/26 17:12
 */
@Getter
public class TablePrimaryKeyInfo {
    /**
     * 表目录(可能为空)
     */
    private String tableCat;

    /**
     * 表模式(可能为空)
     */
    private String tableSchem;

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 字段名
     */
    private String columnName;

    /**
     * 主键内的序列号(值1表示主键的第一列，值2表示主键内的第二列)。
     */
    private short keySeq;

    /**
     * 主键名(可能为空)
     */
    private String pkName;
}
