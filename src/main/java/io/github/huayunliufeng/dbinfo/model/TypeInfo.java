/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import lombok.Getter;

/**
 * 此数据库支持的所有数据类型的描述。
 *
 * @author zhongq
 * @datetime 2024/4/1 17:20
 */
@Getter
public class TypeInfo {
    /**
     * 类型名称
     */
    private String typeName;

    /**
     * SQL数据类型: java.sql.Types
     */
    private int dataType;

    /**
     * 最大精度。
     * <p>precision列表示服务器对给定数据类型支持的最大列大小。</p>
     * <p>对于数字数据，这是最大精度。</p>
     * <p>对于字符数据，这是以字符为单位的长度。</p>
     * <p>对于日期时间数据类型，这是字符串表示的长度（以字符为单位）（假设分数秒组件的最大允许精度）。</p>
     * <p>对于二进制数据，这是以字节为单位的长度。</p>
     * <p>对于ROWID数据类型，这是以字节为单位的长度。</p>
     * <p>对于列大小不适用的数据类型，返回null。</p>
     */
    private int precision;

    /**
     * 用于引用文字的前缀(可能为null)
     */
    private String literalPrefix;

    /**
     * 用于引用文字的后缀(可能为null)
     */
    private String literalSuffix;

    /**
     * 创建类型时使用的参数(可能为null)
     */
    private String createParams;

    /**
     * 是否可以为null。
     * <ul>
     * <li>typeNoNulls - 不允许NULL值</li>
     * <li>typeNullable - 允许NULL值</li>
     * <li>typeNullableUnknown - 未知</li>
     * </ul>
     */
    private short nullable;

    /**
     * 是否区分大小写。
     */
    private boolean caseSensitive;

    /**
     * 你可以基于这个类型使用“WHERE”吗?
     * <ul>
     * <li>typePredNone - 不支持</li>
     * <li>typePredChar - 仅支持WHERE ..LIKE</li>
     * <li>typePredBasic - 支持除了WHERE ..LIKE</li>
     * <li>typeSearchable - 支持所有WHERE ..</li>
     * </ul>
     */
    private short searchable;

    /**
     * 是否无符号。
     */
    private boolean unsignedAttribute;

    /**
     * 可以是货币值吗?
     */
    private boolean fixedPrecScale;

    /**
     * 是否可以用于自增值。
     */
    private boolean autoIncrement;

    /**
     * 类型名称的本地化版本(可能为null)
     */
    private String localTypeName;

    /**
     * 支持的最小规模
     */
    private short minimumScale;

    /**
     * 支持的最大规模
     */
    private short maximumScale;

    /**
     * 未使用
     */
    private int sqlDataType;

    /**
     * 未使用
     */
    private int sqlDatetimeSub;

    /**
     * 通常为2或10
     */
    private int numPrecRadix;
}
