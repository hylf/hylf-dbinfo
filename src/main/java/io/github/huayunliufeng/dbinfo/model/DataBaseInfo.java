/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.model;

import io.github.huayunliufeng.common.annotation.CallClassType;
import io.github.huayunliufeng.common.annotation.ValueFromMethod;
import lombok.Getter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.RowIdLifetime;

/**
 * 数据库信息。
 *
 * @author huayunliufeng
 * @date_time 2024/3/23 13:59
 */
@Getter
public class DataBaseInfo {

    /**
     * 数据库基础信息
     */
    @CallClassType
    private BaseInfo baseInfo;

    /**
     * 数据库扩展信息
     */
    @CallClassType
    private ExtInfo extInfo;

    /**
     * 数据库各种限制信息
     */
    @CallClassType
    private LimitInfo limitInfo;

    /**
     * 支持的操作
     */
    @CallClassType
    private SupportInfo supportInfo;


    @Getter
    public static class BaseInfo {
        /**
         * 获得当前数据库是什么数据库
         */
        private String databaseProductName;

        /**
         * 数据库的版本
         */
        private String databaseProductVersion;

        /**
         * 获取JDBC驱动程序的名称。
         */
        private String driverName;

        /**
         * 驱动程序的版本
         */
        private String driverVersion;

        /**
         * JDBC驱动程序的主版本号。
         */
        private int driverMajorVersion;

        /**
         * JDBC驱动程序的次要版本号。
         */
        private int driverMinorVersion;

        /**
         * 基础数据库的主版本号。
         */
        private int databaseMajorVersion;

        /**
         * 基础数据库的次版本号。
         */
        private int databaseMinorVersion;

        /**
         * 驱动程序的主JDBC版本号。
         */
        @ValueFromMethod("getJDBCMajorVersion")
        private int jdbcMajorVersion;

        /**
         * 驱动程序的次要JDBC版本号。
         */
        @ValueFromMethod("getJDBCMinorVersion")
        private int jdbcMinorVersion;

        /**
         * 检索此DBMS的URL。
         */
        @ValueFromMethod("getURL")
        private String url;

        /**
         * 检索此数据库已知的用户名。
         */
        private String userName;
    }

    @Getter
    public static class ExtInfo {
        /**
         * 检索此数据库用作编目和表名之间分隔符的字符串。
         */
        private String catalogSeparator;

        /**
         * 检索数据库供应商的首选术语“catalog”。
         */
        private String catalogTerm;

        /**
         * 检索可以用于未加引号的标识符名称中的所有“extra”字符(a-z、a-z、0-9和_以外的字符)。
         */
        private String extraNameCharacters;

        /**
         * 检索此数据库的默认事务隔离级别。可能的值在java.sql.Connection中定义。
         */
        private int defaultTransactionIsolation;

        /**
         * 检索用于引用SQL标识符的字符串。如果不支持标识符引号，这个方法返回一个空格。
         */
        private String identifierQuoteString;

        /**
         * 检索此数据库中可用的数学函数的逗号分隔列表。这些是在JDBC函数转义子句中使用的Open /Open CLI数学函数名。
         */
        private String numericFunctions;

        /**
         * 检索数据库供应商的首选术语“procedure”。
         */
        private String procedureTerm;

        /**
         * 检索此数据库对ResultSet对象的默认保持性。
         */
        private int resultSetHoldability;

        /**
         * 指示此数据源是否支持SQL ROWID类型，以及ROWID对象的有效生命周期。
         */
        private RowIdLifetime rowIdLifetime;

        /**
         * 检索数据库供应商对“schema”的首选术语。
         */
        private String schemaTerm;

        /**
         * 检索可用于转义通配符的字符串。这是一个字符串，可以用于转义作为模式的目录搜索参数中的'_'或'%'(因此使用一个通配符)。
         * ` _ `字符表示任何单个字符;'%'字符表示零或多个字符的任何序列。
         */
        private String searchStringEscape;

        /**
         * 检索此数据库的所有SQL关键字的逗号分隔列表，这些关键字不是SQL:2003关键字。
         */
        @ValueFromMethod("getSQLKeywords")
        private String sqlKeywords;

        /**
         * 指示SQLException.getSQLState返回的SQLSTATE是X/Open（现在称为Open Group）SQL CLI还是SQL:2003。
         */
        @ValueFromMethod("getSQLStateType")
        private int sqlStateType;

        /**
         * 检索此数据库可用的字符串函数的逗号分隔列表。这些是JDBC函数转义子句中使用的Open Group CLI字符串函数名。
         */
        private String stringFunctions;

        /**
         * 检索此数据库可用的系统函数的逗号分隔列表。这些是JDBC函数转义子句中使用的Open Group CLI系统函数名。
         */
        private String systemFunctions;

        /**
         * 检索此数据库可用的时间和日期函数的逗号分隔列表。
         */
        private String timeDateFunctions;
    }

    @Getter
    public static class LimitInfo {
        /**
         * 检索此数据库允许在内联二进制字面量中使用的十六进制字符的最大数量。
         */
        private int maxBinaryLiteralLength;

        /**
         * 检索此数据库允许在编目名称中包含的最大字符数。
         */
        private int maxCatalogNameLength;

        /**
         * 检索此数据库允许的字符字面量的最大字符数。
         */
        private int maxCharLiteralLength;

        /**
         * 检索此数据库允许的列名的最大字符数。
         */
        private int maxColumnNameLength;

        /**
         * 检索此数据库允许在GROUP BY子句中包含的最大列数。
         */
        private int maxColumnsInGroupBy;

        /**
         * 检索此数据库允许在索引中包含的最大列数。
         */
        private int maxColumnsInIndex;

        /**
         * 检索此数据库在ORDER BY子句中允许的最大列数。
         */
        private int maxColumnsInOrderBy;

        /**
         * 检索此数据库在SELECT列表中允许的最大列数。
         */
        private int maxColumnsInSelect;

        /**
         * 检索此数据库在表中允许的最大列数。
         */
        private int maxColumnsInTable;

        /**
         * 检索到此数据库可能的最大并发连接数。
         */
        private int maxConnections;

        /**
         * 检索此数据库允许在游标名称中包含的最大字符数。
         */
        private int maxCursorNameLength;

        /**
         * 检索此数据库允许索引的最大字节数，包括索引的所有部分。
         */
        private int maxIndexLength;

        /**
         * 检索此数据库允许LOB的逻辑大小的最大字节数。默认实现将返回0
         */
        private long maxLogicalLobSize;

        /**
         * 检索此数据库允许在程序名称中包含的最大字符数。
         */
        private int maxProcedureNameLength;

        /**
         * 检索此数据库允许在单行中的最大字节数。
         */
        private int maxRowSize;

        /**
         * 检索此数据库允许在模式名称中包含的最大字符数。
         */
        private int maxSchemaNameLength;

        /**
         * 检索此数据库在SQL语句中允许的最大字符数。
         */
        private int maxStatementLength;

        /**
         * 检索此数据库中可同时打开的活动语句的最大数目。
         */
        private int maxStatements;

        /**
         * 检索此数据库允许在表名中包含的最大字符数。
         */
        private int maxTableNameLength;

        /**
         * 检索此数据库在SELECT语句中允许的最大表数。
         */
        private int maxTablesInSelect;

        /**
         * 检索此数据库允许在用户名中包含的最大字符数。
         */
        private int maxUserNameLength;
    }

    @Getter
    public static class SupportInfo {
        /**
         * 检索当前用户是否可以调用 getProcedures 方法返回的所有过程。
         */
        @ValueFromMethod("allProceduresAreCallable")
        private boolean allProceduresAreCallable;

        /**
         * 检索当前用户是否可以在 SELECT 语句中使用 getTables 方法返回的所有表。
         */
        @ValueFromMethod("allTablesAreSelectable")
        private boolean allTablesAreSelectable;

        /**
         * 检索 autoCommit 为 true 时的 SQLException 是否指示所有打开的 ResultSet 都已关闭，即使是可保留的 ResultSet。
         * 当自动提交为 true 时发生 SQLException 时，JDBC 驱动程序是否以提交操作、回滚操作或既不执行提交也不执行回滚来响应是特定于供应商的。
         * 这种差异的一个潜在结果是可保存的结果集是否关闭。
         */
        @ValueFromMethod("autoCommitFailureClosesAllResultSets")
        private boolean autoCommitFailureClosesAllResultSets;

        /**
         * 检索事务中的数据定义语句是否强制事务提交。
         */
        @ValueFromMethod("dataDefinitionCausesTransactionCommit")
        private boolean dataDefinitionCausesTransactionCommit;

        /**
         * 检索此数据库是否忽略事务中的数据定义语句。
         */
        @ValueFromMethod("dataDefinitionIgnoredInTransactions")
        private boolean dataDefinitionIgnoredInTransactions;

        /**
         * 检索方法 getMaxRowSize 的返回值是否包含 SQL 数据类型 LONGVARCHAR 和 LONGVARBINARY。
         */
        @ValueFromMethod("doesMaxRowSizeIncludeBlobs")
        private boolean doesMaxRowSizeIncludeBlobs;

        /**
         * 检索如果为自动生成的键列指定的列名或索引有效且语句成功，是否始终返回生成的键。 返回的密钥可能基于也可能不基于自动生成的密钥的列。
         * 有关更多详细信息，请参阅 JDBC 驱动程序文档。
         */
        @ValueFromMethod("generatedKeyAlwaysReturned")
        private boolean generatedKeyAlwaysReturned;

        /**
         * 检索目录是否出现在完全限定表名的开头。 如果没有，目录将出现在末尾。
         */
        @ValueFromMethod("isCatalogAtStart")
        private boolean isCatalogAtStart;

        /**
         * 检索此数据库是否处于只读模式。
         */
        @ValueFromMethod("isReadOnly")
        private boolean isReadOnly;

        /**
         * 指示对 LOB 进行的更新是在副本上进行还是直接对 LOB 进行。
         */
        @ValueFromMethod("locatorsUpdateCopy")
        private boolean locatorsUpdateCopy;

        /**
         * 检索此数据库是否支持 NULL 和非 NULL 值之间的串联。
         */
        @ValueFromMethod("nullPlusNonNullIsNull")
        private boolean nullPlusNonNullIsNull;

        /**
         * 检索 NULL 值是否在末尾排序，无论排序顺序如何。
         */
        @ValueFromMethod("nullsAreSortedAtEnd")
        private boolean nullsAreSortedAtEnd;

        /**
         * 检索 NULL 值是否在开始时排序，无论排序顺序如何。
         */
        @ValueFromMethod("nullsAreSortedAtStart")
        private boolean nullsAreSortedAtStart;

        /**
         * 检索 NULL 值是否排序较高。 高排序意味着 NULL 值比域中的任何其他值排序更高。 按升序排列，如果此方法返回 true，则 NULL 值将出现在末尾。
         * 相比之下，方法 nullsAreSortedAtEnd 指示 NULL 值是否在末尾排序，无论排序顺序如何。
         */
        @ValueFromMethod("nullsAreSortedHigh")
        private boolean nullsAreSortedHigh;

        /**
         * 检索 NULL 值是否排序较低。 排序较低意味着 NULL 值排序低于域中的任何其他值。 按升序排列，如果此方法返回 true，则 NULL 值将出现在开头。
         * 相比之下，方法 nullsAreSortedAtStart 指示 NULL 值是否在开头排序，无论排序顺序如何。
         */
        @ValueFromMethod("nullsAreSortedLow")
        private boolean nullsAreSortedLow;

        /**
         * 检索对于给定类型的 ResultSet 对象，结果集自身的更新是否可见。
         */
        @ValueFromMethod(value = "ownUpdatesAreVisible", paramTypes = int.class, groupNumber = 3, intValues = {ResultSet.TYPE_FORWARD_ONLY, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_SCROLL_SENSITIVE})
        private boolean[] ownUpdatesAreVisible;

        /**
         * 检索结果集自身的插入是否可见。
         */
        @ValueFromMethod(value = "ownInsertsAreVisible", paramTypes = int.class, groupNumber = 3, intValues = {ResultSet.TYPE_FORWARD_ONLY, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_SCROLL_SENSITIVE})
        private boolean[] ownInsertsAreVisible;

        /**
         * 检索结果集本身的删除是否可见。
         */
        @ValueFromMethod(value = "ownDeletesAreVisible", paramTypes = int.class, groupNumber = 3, intValues = {ResultSet.TYPE_FORWARD_ONLY, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_SCROLL_SENSITIVE})
        private boolean[] ownDeletesAreVisible;

        /**
         * 通过调用ResultSet.rowDeleted方法获取是否可以检测到可见的行删除。如果deletesAreDetected方法返回false，意味着删除的行从结果集中删除了。
         */
        @ValueFromMethod(value = "deletesAreDetected", paramTypes = int.class, groupNumber = 3, intValues = {ResultSet.TYPE_FORWARD_ONLY, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_SCROLL_SENSITIVE})
        private boolean[] deletesAreDetected;

        /**
         * 通过调用ResultSet.rowInserted方法来获取是否可以检测到可见的行插入。
         */
        @ValueFromMethod(value = "insertsAreDetected", paramTypes = int.class, groupNumber = 3, intValues = {ResultSet.TYPE_FORWARD_ONLY, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_SCROLL_SENSITIVE})
        private boolean[] insertsAreDetected;

        /**
         * 通过调用ResultSet.rowUpdated方法来检索是否可以检测到可见的行更新。
         */
        @ValueFromMethod(value = "updatesAreDetected", paramTypes = int.class, groupNumber = 3, intValues = {ResultSet.TYPE_FORWARD_ONLY, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_SCROLL_SENSITIVE})
        private boolean[] updatesAreDetected;

        /**
         * 检索他人插入的数据是否可见。
         */
        @ValueFromMethod(value = "othersInsertsAreVisible", paramTypes = int.class, groupNumber = 3, intValues = {ResultSet.TYPE_FORWARD_ONLY, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_SCROLL_SENSITIVE})
        private boolean[] othersInsertsAreVisible;

        /**
         * 检索其他人所做的更新是否可见。
         */
        @ValueFromMethod(value = "othersUpdatesAreVisible", paramTypes = int.class, groupNumber = 3, intValues = {ResultSet.TYPE_FORWARD_ONLY, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_SCROLL_SENSITIVE})
        private boolean[] othersUpdatesAreVisible;

        /**
         * 检索其他人所做的删除是否可见。
         */
        @ValueFromMethod(value = "othersDeletesAreVisible", paramTypes = int.class, groupNumber = 3, intValues = {ResultSet.TYPE_FORWARD_ONLY, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.TYPE_SCROLL_SENSITIVE})
        private boolean[] othersDeletesAreVisible;

        /**
         * 检索此数据库是否将大小写混合的不带引号的 SQL 标识符视为不区分大小写并以小写形式存储它们。
         */
        @ValueFromMethod("storesLowerCaseIdentifiers")
        private boolean storesLowerCaseIdentifiers;

        /**
         * 检索此数据库是否将混合大小写的带引号的 SQL 标识符视为不区分大小写并以小写形式存储它们。
         */
        @ValueFromMethod("storesLowerCaseQuotedIdentifiers")
        private boolean storesLowerCaseQuotedIdentifiers;

        /**
         * 检索此数据库是否将大小写混合且不带引号的 SQL 标识符视为不区分大小写并将其存储为大小写混合。
         */
        @ValueFromMethod("storesMixedCaseIdentifiers")
        private boolean storesMixedCaseIdentifiers;

        /**
         * 检索此数据库是否将混合大小写引用的 SQL 标识符视为不区分大小写并以混合大小写形式存储它们。
         */
        @ValueFromMethod("storesMixedCaseQuotedIdentifiers")
        private boolean storesMixedCaseQuotedIdentifiers;

        /**
         * 检索此数据库是否将大小写混合的不带引号的 SQL 标识符视为不区分大小写并以大写形式存储它们。
         */
        @ValueFromMethod("storesUpperCaseIdentifiers")
        private boolean storesUpperCaseIdentifiers;

        /**
         * 检索此数据库是否将混合大小写的带引号的 SQL 标识符视为不区分大小写并以大写形式存储它们。
         */
        @ValueFromMethod("storesUpperCaseQuotedIdentifiers")
        private boolean storesUpperCaseQuotedIdentifiers;

        /**
         * 检索此数据库是否支持带有添加列的 ALTER TABLE。
         */
        @ValueFromMethod("supportsAlterTableWithAddColumn")
        private boolean supportsAlterTableWithAddColumn;

        /**
         * 检索此数据库是否支持带有删除列的 ALTER TABLE。
         */
        @ValueFromMethod("supportsAlterTableWithDropColumn")
        private boolean supportsAlterTableWithDropColumn;

        /**
         * 检索此数据库是否支持 ANSI92 入门级 SQL 语法。
         */
        @ValueFromMethod("supportsANSI92EntryLevelSQL")
        private boolean supportsANSI92EntryLevelSQL;

        /**
         * 检索此数据库是否支持支持的 ANSI92 完整 SQL 语法。
         */
        @ValueFromMethod("supportsANSI92FullSQL")
        private boolean supportsANSI92FullSQL;

        /**
         * 检索该数据库是否支持所支持的 ANSI92 中间 SQL 语法。
         */
        @ValueFromMethod("supportsANSI92IntermediateSQL")
        private boolean supportsANSI92IntermediateSQL;

        /**
         * 检索此数据库是否支持批量更新。
         */
        @ValueFromMethod("supportsBatchUpdates")
        private boolean supportsBatchUpdates;

        /**
         * 检索是否可以在数据操作语句中使用目录名称。
         */
        @ValueFromMethod("supportsCatalogsInDataManipulation")
        private boolean supportsCatalogsInDataManipulation;

        /**
         * 检索是否可以在索引定义语句中使用目录名称。
         */
        @ValueFromMethod("supportsCatalogsInIndexDefinitions")
        private boolean supportsCatalogsInIndexDefinitions;

        /**
         * 检索是否可以在权限定义语句中使用目录名称。
         */
        @ValueFromMethod("supportsCatalogsInPrivilegeDefinitions")
        private boolean supportsCatalogsInPrivilegeDefinitions;

        /**
         * 检索是否可以在过程调用语句中使用目录名称。
         */
        @ValueFromMethod("supportsCatalogsInProcedureCalls")
        private boolean supportsCatalogsInProcedureCalls;

        /**
         * 检索是否可以在表定义语句中使用编目名称。
         */
        @ValueFromMethod("supportsCatalogsInTableDefinitions")
        private boolean supportsCatalogsInTableDefinitions;

        /**
         * 检索此数据库是否支持列别名。
         * 如果是这样，可以使用SQL AS子句为计算列提供名称，或者根据需要为列提供别名。
         */
        @ValueFromMethod("supportsColumnAliasing")
        private boolean supportsColumnAliasing;

        /**
         * 检索此数据库是否支持JDBC标量函数CONVERT，用于将一种JDBC类型转换为另一种JDBC类型。JDBC类型是定义在java.sql.Types中的通用SQL数据类型。
         */
        @ValueFromMethod("supportsConvert")
        private boolean supportsConvert;

        /**
         * 检索此数据库是否支持ODBC Core SQL语法。
         */
        @ValueFromMethod("supportsCoreSQLGrammar")
        private boolean supportsCoreSQLGrammar;

        /**
         * 检索此数据库是否支持相关子查询。
         */
        @ValueFromMethod("supportsCorrelatedSubqueries")
        private boolean supportsCorrelatedSubqueries;

        /**
         * 检索此数据库是否同时支持事务中的数据定义和数据操作语句。
         */
        @ValueFromMethod("supportsDataDefinitionAndDataManipulationTransactions")
        private boolean supportsDataDefinitionAndDataManipulationTransactions;

        /**
         * 检索此数据库是否只支持事务中的数据操作语句。
         */
        @ValueFromMethod("supportsDataManipulationTransactionsOnly")
        private boolean supportsDataManipulationTransactionsOnly;

        /**
         * 检索在支持表关联名称时，是否将它们限制为与表的名称不同。
         */
        @ValueFromMethod("supportsDifferentTableCorrelationNames")
        private boolean supportsDifferentTableCorrelationNames;

        /**
         * 按列表顺序检索此数据库是否支持表达式。
         */
        @ValueFromMethod("supportsExpressionsInOrderBy")
        private boolean supportsExpressionsInOrderBy;

        /**
         * 检索此数据库是否支持ODBC扩展SQL语法。
         */
        @ValueFromMethod("supportsExtendedSQLGrammar")
        private boolean supportsExtendedSQLGrammar;

        /**
         * 检索此数据库是否支持完全嵌套的外联接。
         */
        @ValueFromMethod("supportsFullOuterJoins")
        private boolean supportsFullOuterJoins;

        /**
         * 检索在语句执行后是否可以检索自动生成的键
         */
        @ValueFromMethod("supportsGetGeneratedKeys")
        private boolean supportsGetGeneratedKeys;

        /**
         * 检索此数据库是否支持某种形式的GROUP BY子句。
         */
        @ValueFromMethod("supportsGroupBy")
        private boolean supportsGroupBy;

        /**
         * 检索数据库是否支持在GROUP BY子句中使用SELECT语句中不包含的列，前提是SELECT语句中的所有列都包含在GROUP BY子句中。
         */
        @ValueFromMethod("supportsGroupByBeyondSelect")
        private boolean supportsGroupByBeyondSelect;

        /**
         * 检索此数据库是否支持使用不在GROUP BY子句中的SELECT语句中的列。
         */
        @ValueFromMethod("supportsGroupByUnrelated")
        private boolean supportsGroupByUnrelated;

        /**
         * 检索此数据库是否支持SQL完整性增强功能。
         */
        @ValueFromMethod("supportsIntegrityEnhancementFacility")
        private boolean supportsIntegrityEnhancementFacility;

        /**
         * 检索此数据库是否支持指定类似的转义子句。
         */
        @ValueFromMethod("supportsLikeEscapeClause")
        private boolean supportsLikeEscapeClause;

        /**
         * 检索此数据库是否为外连接提供有限支持。(如果方法supportsFullOuterJoins返回true，则返回true)。
         */
        @ValueFromMethod("supportsLimitedOuterJoins")
        private boolean supportsLimitedOuterJoins;

        /**
         * 检索此数据库是否支持ODBC最小SQL语法。
         */
        @ValueFromMethod("supportsMinimumSQLGrammar")
        private boolean supportsMinimumSQLGrammar;

        /**
         * 检索此数据库是否将混合大小写未加引号的SQL标识符视为区分大小写的，从而将它们存储为混合大小写。
         */
        @ValueFromMethod("supportsMixedCaseIdentifiers")
        private boolean supportsMixedCaseIdentifiers;

        /**
         * 检索此数据库是否将混合引号的SQL标识符视为区分大小写的，并将其存储为混合大小写。
         */
        @ValueFromMethod("supportsMixedCaseQuotedIdentifiers")
        private boolean supportsMixedCaseQuotedIdentifiers;

        /**
         * 获取是否可以同时从CallableStatement对象返回多个ResultSet对象。
         */
        @ValueFromMethod("supportsMultipleOpenResults")
        private boolean supportsMultipleOpenResults;

        /**
         * 检索此数据库是否支持从对方法execute的单个调用中获取多个ResultSet对象。
         */
        @ValueFromMethod("supportsMultipleResultSets")
        private boolean supportsMultipleResultSets;

        /**
         * 检索此数据库是否允许同时(在不同的连接上)打开多个事务。
         */
        @ValueFromMethod("supportsMultipleTransactions")
        private boolean supportsMultipleTransactions;

        /**
         * 检索此数据库是否支持可调用语句的命名参数。
         */
        @ValueFromMethod("supportsNamedParameters")
        private boolean supportsNamedParameters;

        /**
         * 检索此数据库中的列是否可以定义为非空列。
         */
        @ValueFromMethod("supportsNonNullableColumns")
        private boolean supportsNonNullableColumns;

        /**
         * 检索此数据库是否支持跨提交保持游标打开。
         */
        @ValueFromMethod("supportsOpenCursorsAcrossCommit")
        private boolean supportsOpenCursorsAcrossCommit;

        /**
         * 检索此数据库是否支持跨回滚保持游标打开。
         */
        @ValueFromMethod("supportsOpenCursorsAcrossRollback")
        private boolean supportsOpenCursorsAcrossRollback;

        /**
         * 检索此数据库是否支持跨提交保持语句打开。
         */
        @ValueFromMethod("supportsOpenStatementsAcrossCommit")
        private boolean supportsOpenStatementsAcrossCommit;

        /**
         * 检索此数据库是否支持跨回滚保持语句打开。
         */
        @ValueFromMethod("supportsOpenStatementsAcrossRollback")
        private boolean supportsOpenStatementsAcrossRollback;

        /**
         * 在ORDER BY子句中检索数据库是否支持使用不在SELECT语句中的列。
         */
        @ValueFromMethod("supportsOrderByUnrelated")
        private boolean supportsOrderByUnrelated;

        /**
         * 检索此数据库是否支持某种形式的外连接。
         */
        @ValueFromMethod("supportsOuterJoins")
        private boolean supportsOuterJoins;

        /**
         * 检索此数据库是否支持定位DELETE语句。
         */
        @ValueFromMethod("supportsPositionedDelete")
        private boolean supportsPositionedDelete;

        /**
         * 检索此数据库是否支持定位更新语句。
         */
        @ValueFromMethod("supportsPositionedUpdate")
        private boolean supportsPositionedUpdate;

        /**
         * 检索此数据库是否支持REF游标。
         * 默认实现将返回false。
         */
        @ValueFromMethod("supportsRefCursors")
        private boolean supportsRefCursors;

        /**
         * 检索此数据库是否支持给定的结果集可持有性。
         */
        @ValueFromMethod(value = "supportsResultSetHoldability", paramTypes = int.class, groupNumber = 2, intValues = {ResultSet.HOLD_CURSORS_OVER_COMMIT, ResultSet.CLOSE_CURSORS_AT_COMMIT})
        private boolean[] supportsResultSetHoldability;

        /**
         * 检索此数据库是否支持保存点。
         */
        @ValueFromMethod("supportsSavepoints")
        private boolean supportsSavepoints;

        /**
         * 检索是否可以在数据操作语句中使用模式名称。
         */
        @ValueFromMethod("supportsSchemasInDataManipulation")
        private boolean supportsSchemasInDataManipulation;

        /**
         * 检索是否可以在索引定义语句中使用模式名称。
         */
        @ValueFromMethod("supportsSchemasInIndexDefinitions")
        private boolean supportsSchemasInIndexDefinitions;

        /**
         * 检索是否可以在权限定义语句中使用模式名称。
         */
        @ValueFromMethod("supportsSchemasInPrivilegeDefinitions")
        private boolean supportsSchemasInPrivilegeDefinitions;

        /**
         * 检索是否可以在过程调用语句中使用架构名称。
         */
        @ValueFromMethod("supportsSchemasInProcedureCalls")
        private boolean supportsSchemasInProcedureCalls;

        /**
         * 检索是否可以在表定义语句中使用模式名称。
         */
        @ValueFromMethod("supportsSchemasInTableDefinitions")
        private boolean supportsSchemasInTableDefinitions;

        /**
         * 检索此数据库是否支持SELECT FOR UPDATE语句。
         */
        @ValueFromMethod("supportsSelectForUpdate")
        private boolean supportsSelectForUpdate;

        /**
         * 获取数据库是否支持分片。
         */
        @ValueFromMethod("supportsSharding")
        private boolean supportsSharding;

        /**
         * 检索此数据库是否支持语句池。
         */
        @ValueFromMethod("supportsStatementPooling")
        private boolean supportsStatementPooling;

        /**
         * 检索此数据库是否支持使用存储过程转义语法调用用户定义函数或供应商函数。
         */
        @ValueFromMethod("supportsStoredFunctionsUsingCallSyntax")
        private boolean supportsStoredFunctionsUsingCallSyntax;

        /**
         * 检索此数据库是否支持使用存储过程转义语法的存储过程调用。
         */
        @ValueFromMethod("supportsStoredProcedures")
        private boolean supportsStoredProcedures;

        /**
         * 检索此数据库是否支持比较表达式中的子查询。
         */
        @ValueFromMethod("supportsSubqueriesInComparisons")
        private boolean supportsSubqueriesInComparisons;

        /**
         * 检索此数据库是否支持EXISTS表达式中的子查询。
         */
        @ValueFromMethod("supportsSubqueriesInExists")
        private boolean supportsSubqueriesInExists;

        /**
         * 检索此数据库是否支持in表达式中的子查询。
         */
        @ValueFromMethod("supportsSubqueriesInIns")
        private boolean supportsSubqueriesInIns;

        /**
         * 检索此数据库是否支持量化表达式中的子查询。
         */
        @ValueFromMethod("supportsSubqueriesInQuantifieds")
        private boolean supportsSubqueriesInQuantifieds;

        /**
         * 检索此数据库是否支持表关联名称。
         */
        @ValueFromMethod("supportsTableCorrelationNames")
        private boolean supportsTableCorrelationNames;

        /**
         * 检索此数据库是否支持给定的事务隔离级别。
         */
        @ValueFromMethod(value = "supportsTransactionIsolationLevel", paramTypes = int.class, groupNumber = 5, intValues = {Connection.TRANSACTION_NONE, Connection.TRANSACTION_READ_UNCOMMITTED, Connection.TRANSACTION_READ_COMMITTED, Connection.TRANSACTION_REPEATABLE_READ, Connection.TRANSACTION_SERIALIZABLE})
        private boolean[] supportsTransactionIsolationLevel;

        /**
         * 检索此数据库是否支持事务。否则，调用commit方法是noop，隔离级别是TRANSACTION_NONE。
         */
        @ValueFromMethod("supportsTransactions")
        private boolean supportsTransactions;

        /**
         * 检索此数据库是否支持SQL UNION。
         */
        @ValueFromMethod("supportsUnion")
        private boolean supportsUnion;

        /**
         * 检索此数据库是否支持SQL UNION ALL。
         */
        @ValueFromMethod("supportsUnionAll")
        private boolean supportsUnionAll;

        /**
         * 检索此数据库是否为每个表使用一个文件。
         */
        @ValueFromMethod("usesLocalFilePerTable")
        private boolean usesLocalFilePerTable;

        // boolean supportsResultSetConcurrency(int type, int concurrency)
        // boolean supportsResultSetType(int type)
        // boolean supportsConvert(int fromType, int toType)
        /**
         * 检索此数据库是否将表存储在本地文件中。
         */
        @ValueFromMethod("usesLocalFiles")
        private boolean usesLocalFiles;
    }
}
