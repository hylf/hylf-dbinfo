/*
 * © 2024 huayunliufeng保留所有权利, 依据MIT许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package io.github.huayunliufeng.dbinfo.dbinfo;

import io.github.huayunliufeng.common.entity.ConnectionProp;
import io.github.huayunliufeng.dbinfo.constant.TableType;
import io.github.huayunliufeng.dbinfo.model.*;
import io.github.huayunliufeng.dbinfo.utils.HylfConnectionUtil;
import io.github.huayunliufeng.dbinfo.utils.ReadConfigUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author huayunliufeng
 * @date_time 2024/3/24 0:08
 * @desc 文件说明
 */
class ObtainDbInfoTest {

    private static Connection conn;
    private static ConnectionProp prop;

    @BeforeAll
    public static void beforeAll() {
        prop = ReadConfigUtil.readDatasource("mysql-local");
        conn = HylfConnectionUtil.getConnection(prop);
    }

    @AfterAll
    public static void afterAll() throws Exception {
        prop = null;
        conn.close();
        conn = null;
    }

    @Test
    public void testGetDataBaseInfo() throws Exception {
        DataBaseInfo dataBaseInfo = ObtainDbInfo.build(conn).getDataBaseInfo();
        assertNotNull(dataBaseInfo);
    }

    @Test
    public void testGetTableTypes() throws Exception {
        String[] tableTypes = ObtainTableInfo.build(conn).getTableTypesToArray(TableType.LOCAL_TEMPORARY.getCode());
        assertNotNull(tableTypes);
    }

    @Test
    public void testGetCatalogs() throws Exception {
        List<String> allCatalogs = ObtainDbInfo.build(conn).getCatalogs();
        String[] allCatalogArray = ObtainDbInfo.build(conn).getCatalogsToArray("mysql");
        assertFalse(allCatalogs.isEmpty());
        assertTrue(allCatalogArray.length > 0);

    }

    @Test
    public void testGetSchemas() throws Exception {
        List<SchemaInfo> schemaInfos = ObtainDbInfo.build(conn).getSchemas();
        assertNotNull(schemaInfos);
    }

    @Test
    public void testGetTableInfos() throws Exception {
        List<TableInfo> tableInfos = ObtainTableInfo.build(conn).getTables("%");
        assertNotNull(tableInfos);
    }

    @Test
    public void testGetTypeInfo() throws Exception {
        List<TypeInfo> typeInfoList = ObtainDbInfo.build(conn).getTypeInfo();
        assertNotNull(typeInfoList);
    }

    @Test
    public void testGetClientInfos() throws Exception {
        List<ClientInfoProperties> clientInfoPropertiesList = ObtainDbInfo.build(conn).getClientInfoProperties();
        assertNotNull(clientInfoPropertiesList);
    }

    @Test
    public void testGetFunctions() throws Exception {
        List<FunctionsInfo> functionsInfoList = ObtainFunctionsInfo.build(conn).getFunctions("%");
        assertNotNull(functionsInfoList);
    }

    @Test
    public void testGetPseudoColumns() throws Exception {
        List<PseudoColumnsInfo> pseudoColumnsInfoList = ObtainPseudoColumnsInfo.build(conn).getPseudoColumns("%", "%");
        assertNotNull(pseudoColumnsInfoList);
    }

    @Test
    public void testGetProcedures() throws Exception {
        List<ProceduresInfo> proceduresInfoList = ObtainProceduresInfo.build(conn).getProcedures("%");
        assertNotNull(proceduresInfoList);
    }
}